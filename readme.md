# Docker LAMP

This is a basic Drupal environment using MariaDB.

## Usage 

```
$ docker-compose up 
```

Access from localhost:8083/

Copy https://github.com/drupal/drupal/blob/10.1.x/sites/default/default.settings.php to:

./drupal_sites/default/settings.php
./drupal_sites/default/default.settings.php

Ensure to put 'mariadb' in host name for db settings (under advanced)

Take username and pw from 

See https://www.linode.com/docs/quick-answers/linux/drupal-with-docker-compose/